var playState = {

    create: function() {

        // BACKGROUND
        sky = game.add.sprite(0, 0, 'bgLevel');



        // HEART  vie
        hearts = game.add.group();

        for (let b = 0; b < live; b++) {
            heart = hearts.create(600 + b * 35, 10, 'heart');
            heart.fixedToCamera = true;
        }

        // DOOR
        doors = game.add.group();
        door = doors.create(0, 470, 'door');
        door2 = doors.create(720, 470, 'door');
        door.enableBody = true;
        door2.enableBody = true;
        door2.animations.add('Open', [0, 1, 2, 3], 20, true);
        door.animations.add('Open', [0, 1, 2, 3], 20, true);

        // PLATEFORMS
        platforms = game.add.group();
        platforms.enableBody = true; //
        var ground = platforms.create(0, game.world.height - 53, 'ground');
        ground.body.immovable = true;
        //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
        ground.scale.setTo(20, 1);
        ground.body.immovable = true;

        // JOUEUR
        player = game.add.sprite(32, game.world.height - 150, 'dude'); // AJOUTE LE PERSO
        game.physics.arcade.enable(player); // CREATION DE LA COLISION
        player.body.bounce.y = 0.2; // REBONT
        player.body.gravity.y = 300; // GRAVITE
        player.body.collideWorldBounds = true; // CREER LE REBONT DURANT LA COLISION


        player.animations.add('left', [0, 1, 2, 3], 10, true); // ANIMATION PERSO DROITE
        player.animations.add('right', [7, 8, 9, 10], 10, true); // ANIMATION PERSO GAUCHE

        // ETOILES

        stars = game.add.group();
        stars.enableBody = true;

        //  CREATION DE 12 ETOILES
        for (let i = 0; i < 25; i++) {
            let star = stars.create(50 + i * 90, 0, 'star'); // PLACEMENT DES ETOILES
            //  GRAVITE DES ETOILES
            star.body.gravity.y = 300;
            //  PLACEMENT DES ETOILES ALEATOIREMENT
            star.body.bounce.y = 0.1 + Math.random() * 0.2;
        }

        // SCORE
        scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '20px', fill: '#ffffff' });
        scoreText.fixedToCamera = true;


        // cursors
        cursors = game.input.keyboard.createCursorKeys(); // GESTION DU CLAnkeyR

        // KEYS
        keys = game.add.group();
        keys.enableBody = true;
        key1 = keys.create(370, 490, 'key');



    },

    update: function() {
        let hitPlatform = game.physics.arcade.collide(player, platforms); // COLISION PLATEFORME ET JOUEUR
        game.physics.arcade.collide(stars, platforms); // COLISION ETOILE ET PLATEFORME
        game.physics.arcade.collide(player, keys, collectKey, null, this); // COLISION JOUEUR ET CLEF
        game.physics.arcade.overlap(player, stars, collectStar, null, this); // COLISION JOUEUR ET ETOILE

        player.body.velocity.x = 0;
        if (cursors.left.isDown) // QUAND LA FLECHE BAS EST APPUYEE
        {
            player.body.velocity.x = -150;
            player.animations.play('left');
            game.camera.x -= 4;
        } else if (cursors.right.isDown) {
            player.body.velocity.x = 150;
            player.animations.play('right');
            game.camera.x += 2;

        } else {
            player.animations.stop();
            player.frame = 4;
        }

        if (cursors.up.isDown && player.body.touching.down && hitPlatform) {
            player.body.velocity.y = -350;

        }

        // COLLECT DES ETOILES
        function collectStar(player, star, key1) {
            star.kill(); // EFFACE LES ETOILES DU JEU
            score += 10;
            scoreText.text = 'Score : ' + score;

        }

        // COLLECT CLEF
        function collectKey(player, key1) {
            key1.kill();
            kscore += 1;
            score += 100;
            scoreText.text = 'Score : ' + score;
        }

        console.log(kscore);

        if (checkCollideDoor(player, door2)) {
            door2.animations.play('Open');

            game.state.start('win');

        }

        function checkCollideDoor(player, door2) {
            var bounds1 = player.getBounds();
            var bounds2 = door2.getBounds();
            return Phaser.Rectangle.intersects(bounds1, bounds2);
            game.state.start('win');

        }
    }

}
