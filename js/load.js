var loadState = {
  preload: function() {
    var texte = game.add.text(270, 250, 'Loading', { font: "60px Roboto", fill: "#fff" });
    game.load.image('bgLevel', 'gallery/bgLevel.png');
    game.load.image('bgGameOver', 'gallery/bgloose.jpg');
    game.load.image('bgWin', 'gallery/bgWin.png');
    game.load.image('star', 'gallery/star.png');

    game.load.image('ground', 'gallery/ground.jpg');

    game.load.spritesheet('door', 'gallery/porte.png', 63, 85);
    game.load.image('key', 'gallery/key1.png', 32, 32);
    game.load.image('heart', 'gallery/heart1.png');
    game.load.image('star', 'gallery/star.png');

    game.load.spritesheet('dude', 'gallery/dude.png', 32, 48);
    game.load.spritesheet('monstres', 'gallery/monstre.png', 82, 52);
  },

  create: function() {
    setTimeout(function() {
      game.state.start("menu");
    }, 1000);

  },

};
