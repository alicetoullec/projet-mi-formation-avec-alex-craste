var looseState = {
    create: function() {
        sky = game.add.sprite(50, 50, 'bgGameOver');

        score = 0;
        kscore = 0;
        live = 3;
        var textReplay = game.add.text(280, 500, 'press the "R" to restart', { font: '25px Roboto', fill: 'lightpink' });
        var rkey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        rkey.onDown.addOnce(this.restart, this);
    },
    restart: function() {
        game.state.start('level1');
    },
}
