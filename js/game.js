let game = new Phaser.Game(800, 600, Phaser.AUTO, 'gamediv'),
    Main = function() {},
    gameOptions = {
        playSound: true,
        playMusic: true
    },
    musicPlayer;
let niveau;
let score = 0;
let scoreText;
let stars;
let keys;
let key1;
let kscore = 0;
let gameoverText;
let gameoverP;
let live = 1;
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('level1', playState);
game.state.add('level2', level2State);
game.state.add('level3', level3State);
game.state.add('loose', looseState);
game.state.add('win', winState);

game.state.start('boot');