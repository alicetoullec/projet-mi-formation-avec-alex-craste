var menuState = {

    preload: function() {

        var nameLabel = game.add.text(230, 250, 'Wanna play ?', { font: '55px Roboto', fill: 'lightblue' });
        var startLabel = game.add.text(270, 310, 'press the "A" to start', { font: '25px Roboto', fill: '#ffffff' });
        var pkey = game.input.keyboard.addKey(Phaser.Keyboard.A);
        pkey.onDown.addOnce(this.start, this);
    },
    start: function() {
        game.state.start('level3');
        // BACKGROUND
    },
};
